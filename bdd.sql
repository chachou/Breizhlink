-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- H�te : 127.0.0.1
-- G�n�r� le :  mar. 24 juil. 2018 � 17:09
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de donn�es :  `project_breizhlink`
--

-- --------------------------------------------------------

--
-- Structure de la table `multipass`
--

CREATE TABLE `multipass` (
  `url` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `parametre`
--

CREATE TABLE `parametre` (
  `url` varchar(50) NOT NULL,
  `captcha` tinyint(1) NOT NULL,
  `nb_click` int(11) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `statistique`
--

CREATE TABLE `statistique` (
  `id_stats` int(11) NOT NULL,
  `url` varchar(50) NOT NULL,
  `date_stats` date NOT NULL,
  `nb_vues` int(11) NOT NULL,
  `nb_ip` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `url`
--

CREATE TABLE `url` (
  `url` varchar(50) NOT NULL,
  `url_racc` varchar(50) NOT NULL,
  `date_crea` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `url`
--

INSERT INTO `url` (`url`, `url_racc`, `date_crea`) VALUES
('https://test.com/urltroplongue', 'AAABCE', '2018-04-17'),
('http://www.google.fr', 'AABAAE', '2018-07-11');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `login` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `user`
--

INSERT INTO `user` (`login`, `email`, `password`) VALUES
('root', 'root@root.com', 'root'),
('log', 'jojo@jojo.com', 'coucou'),
('chachou', 'ch@chou.chou', 'chachou'),

--
-- Index pour les tables d�charg�es
--

--
-- Index pour la table `statistique`
--
ALTER TABLE `statistique`
  ADD PRIMARY KEY (`id_stats`);

--
-- AUTO_INCREMENT pour les tables d�charg�es
--

--
-- AUTO_INCREMENT pour la table `statistique`
--
ALTER TABLE `statistique`
  MODIFY `id_stats` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;